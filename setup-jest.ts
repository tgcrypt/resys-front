//  incluir /setup-jest para resolver o error,
//  zone-testing.js is needed for the fakeAsync() test helper but could not be found.
// Please make sure that your environment includes zone.js/testing
// import 'jest-preset-angular';
import 'jest-preset-angular/setup-jest';

/* global mocks for jsdom */
const mock = () => {
  let storage: { [key: string]: string } = {};
  return {
    getItem: (key: string) => (key in storage ? storage[key] : null),
    setItem: (key: string, value: string) => (storage[key] = value),
    removeItem: (key: string) => delete storage[key],
    clear: () => (storage = {})
  };
};

Object.defineProperty(window, 'localStorage', { value: mock() });
Object.defineProperty(window, 'sessionStorage', { value: mock() });
Object.defineProperty(window, 'getComputedStyle', {
  value: () => ['-webkit-appearance']
});

Object.defineProperty(document.body.style, 'transform', {
  value: () => {
    return {
      enumerable: true,
      configurable: true
    };
  }
});

/* output shorter and more meaningful Zone error stack traces */
// Error.stackTraceLimit = 2;
