import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/services/admin/auth/auth.service';
import { UtilsService } from '../../shared/services/utils/utils.service';

@Injectable()
export class AuthGuardAdmin implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private utils: UtilsService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.authService.validateAdmin()) {
      return true;
    } else {
      this.utils.toaster(
        'Usuário não tem permissão para essa função!',
        'error'
      );
      return this.router.createUrlTree(['/register']);
    }
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.canActivate(route, state);
  }
}
