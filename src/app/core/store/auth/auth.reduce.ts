import { Action, createReducer, on } from '@ngrx/store';
import * as TokenActions from './auth.actions';

export interface AuthState {
  user: string;
  token: string;
  visible: boolean;
  menu: boolean;
}

const initialState: AuthState = {
  user: 'null',
  token: 'null',
  visible: false,
  menu: true
};

const _authReducer = createReducer(
  initialState,
  on(TokenActions.setToken, (state, action) => ({
    ...state,
    user: action.user,
    token: action.token,
    visible: true
  })),
  on(TokenActions.setMenu, (state, action) => ({
    ...state,
    menu: action.menu
  })),
  on(TokenActions.cleanToken, (state, action) => ({
    ...state,
    user: state.user,
    token: state.token,
    visible: false,
    menu: true
  }))
);

export function authReducer(state: AuthState = initialState, action: Action) {
  return _authReducer(state, action);
}
