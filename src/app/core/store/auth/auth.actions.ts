import { createAction, props } from '@ngrx/store';

export const setToken: any = createAction(
  '[Auth] setToke',
  props<{
    user: string;
    token: string;
    visible: boolean;
    menu: boolean;
  }>()
);
export const setMenu: any = createAction(
  '[Auth] setMenu',
  props<{
    menu: boolean;
  }>()
);

export const cleanToken: any = createAction(
  '[Auth] clearToken',
  props<{
    user: string;
    token: string;
    visible: boolean;
    menu: boolean;
  }>()
);
