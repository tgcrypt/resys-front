import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { UtilsService } from 'src/app/shared/services/utils/utils.service';
import * as AuthActions from '../../core/store/auth/auth.actions';
import { AuthState } from '../store/auth/auth.reduce';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(
    private router: Router,
    private utils: UtilsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const data = this.utils.getLocalStorage();
    if (data) {
      const request = req.clone({
        headers: req.headers.append('Authorization', `Bearer ${data.token}`)
      });
      return next.handle(request).pipe(
        tap(
          _ => {},
          err => {
            if (err.status == 401) {
              this.utils.removeLocalStorage();
              this.store.dispatch(AuthActions.cleanToken());
              this.router.navigateByUrl('sign-in');
            }
          }
        )
      );
    } else {
      return next.handle(req.clone());
    }
  }
}
