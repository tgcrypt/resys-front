import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Unsubscribable } from 'rxjs';
import { AuthState } from 'src/app/core/store/auth/auth.reduce';
import { AuthService } from 'src/app/shared/services/admin/auth/auth.service';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.scss']
})
export class UserDropdownComponent implements OnInit {
  unsubscribe!: Unsubscribable;
  userName!: string;

  constructor(
    public auth: AuthService,
    private store: Store<{ auth: AuthState }>
  ) {}

  ngOnInit(): void {
    this.loadName();
  }

  loadName(): void {
    this.unsubscribe = this.store.select('auth').subscribe(res => {
      this.userName = res.user;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.unsubscribe();
  }
}
