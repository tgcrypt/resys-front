import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Unsubscribable } from 'rxjs';
import { AuthState } from '../../../store/auth/auth.reduce';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() title!: string;
  @Input() visible!: boolean;
  @Output() toggle: EventEmitter<void> = new EventEmitter<void>();
  menuIcon!: boolean;
  unsubscribe!: Unsubscribable;
  constructor(private store: Store<{ auth: AuthState }>) {}

  ngOnInit(): void {
    this.loadState();
  }

  toggleMenu(): void {
    this.toggle.emit();
  }

  loadState() {
    this.unsubscribe = this.store.select('auth').subscribe(res => {
      this.menuIcon = res.visible;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.unsubscribe();
  }
}
