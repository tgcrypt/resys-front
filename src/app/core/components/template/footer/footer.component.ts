import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Unsubscribable } from 'rxjs';
import { AuthState } from 'src/app/core/store/auth/auth.reduce';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  menu!: boolean;
  unsubscribe!: Unsubscribable;
  constructor(private store: Store<{ auth: AuthState }>) {}

  ngOnInit(): void {
    this.loadState();
  }

  loadState() {
    this.unsubscribe = this.store.select('auth').subscribe(res => {
      this.menu = res.visible;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.unsubscribe();
  }
}
