import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContentComponent } from './core/components/template/content/content.component';
import { FooterComponent } from './core/components/template/footer/footer.component';
import { HeaderComponent } from './core/components/template/header/header.component';
import { MenuComponent } from './core/components/template/menu/menu.component';
import { UserDropdownComponent } from './core/components/template/user-dropdown/user-dropdown.component';
import { AuthGuardAdmin } from './core/guards/admin.guard';
import { AuthGuard } from './core/guards/auth.guard';
import { AuthInterceptorService } from './core/services/auth-interceptor.service';
import { authReducer } from './core/store/auth/auth.reduce';
import { SharedModule } from './shared/modules/shared.module';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    ContentComponent,
    UserDropdownComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    StoreModule.forRoot({ auth: authReducer })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    AuthGuard,
    AuthGuardAdmin,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
