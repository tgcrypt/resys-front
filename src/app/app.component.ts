import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { take, Unsubscribable } from 'rxjs';
import * as AuthActions from './core/store/auth/auth.actions';
import { AuthState } from './core/store/auth/auth.reduce';
import { UtilsService } from './shared/services/utils/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('menu', [
      state('in', style({ opacity: 1 })),
      state('out', style({ opacity: 0 })),
      transition(':enter', [style({ opacity: 0 }), animate(350)])
      // transition(':leave', animate(100, style({ opacity: 0 })))
    ])
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'RevSys';
  toggle: boolean = true;
  state: string = 'in';
  menu: boolean = true;
  unsubscribe!: Unsubscribable;

  constructor(
    private store: Store<{ auth: AuthState }>,
    private utils: UtilsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.validateToken();
    this.loadState();
  }

  toggleMenu(): void {
    let toggle = !this.toggle;
    this.store.dispatch(AuthActions.setMenu({ menu: toggle }));
    this.state == 'in' ? (this.state = 'out') : (this.state = 'in');
  }

  loadState(): void {
    this.unsubscribe = this.store.select('auth').subscribe({
      next: res => {
        this.menu = res.visible;
        this.toggle = res.menu;
      },
      error: err => console.error(err)
    });
  }

  validateToken(): void {
    const userData = this.utils.getLocalStorage();
    if (!userData) {
      this.menu = false;
      this.router.navigate(['sign-in']);
    } else {
      this.store.dispatch(
        AuthActions.setToken({ token: userData.token, user: userData.name })
      );
    }

    this.utils
      .post<any>('validate-token', userData)
      .pipe(take(1))
      .subscribe({
        next: res => {
          if (!res) {
            this.menu = false;
            this.utils.removeLocalStorage();
            this.router.navigate(['sign-in']);
          }
        },
        error: err => {
          console.error(err);
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.unsubscribe();
  }
}
