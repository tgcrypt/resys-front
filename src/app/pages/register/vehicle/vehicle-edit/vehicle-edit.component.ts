import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { take } from 'rxjs';
import { Vehicle } from 'src/app/shared/models/register/vehicle.model';
import { VehicleService } from 'src/app/shared/services/register/vehicle.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.scss']
})
export class VehicleEditComponent extends CommonService implements OnInit {
  param!: string | null;
  title!: string;
  checked: boolean = false;
  employeeList: any;

  constructor(public service: VehicleService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
    this.routeParam();
    this.loadSelectEmployee();
  }

  routeParam(): void {
    this.param = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.param && this.param != '0') {
      this.loadData(this.param);
    }
    this.setTitle(this.param);
  }

  setTitle(vl: string | null): void {
    if (vl != '0') {
      this.title = 'Editar Veiculo';
    } else {
      this.title = 'Incluir Veiculo';
    }
  }

  loadData(id: string): void {
    this.spinner.show();
    this.utils
      .get<Vehicle>(`vehicle/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.service.setForm(res);
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          this.spinner.hide();
        }
      });
  }

  loadSelectEmployee(): void {
    this.spinner.show();
    this.utils
      .get<{ empId: number; empName: string }[]>('employee/select')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.employeeList = res;
          this.spinner.hide();
        },
        error: err => {
          this.spinner.hide();
          console.error(err);
          this.utils.toaster('Error ao carregar lista de colaborador', 'error');
        }
      });
  }

  submit(): void {
    this.spinner.show();
    const data: Vehicle = this.service.form.value;
    if (this.param != '0') {
      this.utils
        .put<Vehicle>(`vehicle/${data.vhcId}`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Peça / Serviço salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/vehicle']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          }
        });
    } else {
      this.utils
        .post<Vehicle>(`vehicle`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Peça / Serviço salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/vehicle']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            if (err.error) {
              this.utils.toaster(err.error, 'error');
            } else {
              this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
            }
          }
        });
    }
  }

  isControlInvalid(controlName: string): boolean | undefined {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string): boolean {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }
}
