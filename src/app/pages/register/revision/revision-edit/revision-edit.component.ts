import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormArray } from '@angular/forms';
import { take } from 'rxjs';
import { Revision } from 'src/app/shared/models/register/revision.model';
import { RevisionService } from 'src/app/shared/services/register/revision.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-revision-edit',
  templateUrl: './revision-edit.component.html',
  styleUrls: ['./revision-edit.component.scss']
})
export class RevisionEditComponent extends CommonService implements OnInit {
  param!: string | null;
  title!: string;
  checked: boolean = false;
  employeeList: any;
  vehicleList: any;
  partServiceList: any;

  constructor(public service: RevisionService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
    this.routeParam();
    this.loadEmployee();
    this.loadVehicle();
    this.loadPartService();
  }

  routeParam(): void {
    this.param = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.param && this.param != '0') {
      this.loadData(this.param);
    }
    this.setTitle(this.param);
  }

  setTitle(vl: string | null): void {
    if (vl != '0') {
      this.title = 'Editar Peça / Revisão';
    } else {
      this.title = 'Incluir Peça / Revisão';
    }
  }

  loadData(id: string): void {
    this.spinner.show();
    this.utils
      .get<Revision>(`revision/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.service.setForm(res);
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          this.spinner.hide();
        }
      });
  }

  loadEmployee(): void {
    this.utils
      .get<any>('employee/select')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.employeeList = res;
        },
        error: err => {
          console.error(err);
        }
      });
  }

  loadVehicle(): void {
    this.utils
      .get<any>('vehicle/select')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.vehicleList = res;
        },
        error: err => {
          console.error(err);
        }
      });
  }

  loadPartService(): void {
    this.utils
      .get<any>('part-service/select')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.partServiceList = res;
        },
        error: err => {
          console.error(err);
        }
      });
  }

  submit(): void {
    this.spinner.show();
    const data: Revision = this.service.form.value;
    if (this.param != '0') {
      this.utils
        .put<Revision>(`revision/${data.rvsId}`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Revisão salva com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/revision']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          }
        });
    } else {
      this.utils
        .post<Revision>(`revision`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Revisão salva com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/revision']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            if (err.error) {
              this.utils.toaster(err.error, 'error');
            } else {
              this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
            }
          }
        });
    }
  }

  isControlInvalid(controlName: string): boolean | undefined {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string): boolean {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  get rvsPartServiceId(): FormArray {
    return this.service.form.get('rvsPartServiceId') as FormArray;
  }
}
