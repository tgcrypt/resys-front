import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartServiceEditComponent } from './part-service-edit.component';

describe('PartServiceEditComponent', () => {
  let component: PartServiceEditComponent;
  let fixture: ComponentFixture<PartServiceEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartServiceEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PartServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
