import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { take } from 'rxjs';
import { PartService } from 'src/app/shared/models/register/part-service.model';
import { PartServService } from 'src/app/shared/services/register/partService.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-part-service-edit',
  templateUrl: './part-service-edit.component.html',
  styleUrls: ['./part-service-edit.component.scss']
})
export class PartServiceEditComponent extends CommonService implements OnInit {
  param!: string | null;
  title!: string;
  checked: boolean = false;
  providerList: any;

  constructor(public service: PartServService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
    this.routeParam();
    this.loadProvider();
  }

  routeParam(): void {
    this.param = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.param && this.param != '0') {
      this.loadData(this.param);
    }
    this.setTitle(this.param);
  }

  setTitle(vl: string | null): void {
    if (vl != '0') {
      this.title = 'Editar Peça / Serviço';
    } else {
      this.title = 'Incluir Peça / Serviço';
    }
  }

  loadData(id: string): void {
    this.spinner.show();
    this.utils
      .get<PartService>(`part-service/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.service.setForm(res);
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          this.spinner.hide();
        }
      });
  }

  loadProvider(): void {
    this.utils
      .get<any>('provider/select')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.providerList = res;
        },
        error: err => {
          console.error(err);
        }
      });
  }

  submit(): void {
    this.spinner.show();
    const data: PartService = this.service.form.value;
    if (this.param != '0') {
      this.utils
        .put<PartService>(`part-service/${data.ptsId}`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Peça / Serviço salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/part-service']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          }
        });
    } else {
      this.utils
        .post<PartService>(`part-service`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Peça / Serviço salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/part-service']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            if (err.error) {
              this.utils.toaster(err.error, 'error');
            } else {
              this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
            }
          }
        });
    }
  }

  isControlInvalid(controlName: string): boolean | undefined {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string): boolean {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }
}
