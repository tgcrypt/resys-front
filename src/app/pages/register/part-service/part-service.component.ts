import { Component, Injector, OnInit } from '@angular/core';
import { debounceTime, distinctUntilChanged, take } from 'rxjs';
import { PartServiceList } from 'src/app/shared/models/register/part-service.model';
import { PartServService } from 'src/app/shared/services/register/partService.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-part-service',
  templateUrl: './part-service.component.html',
  styleUrls: ['./part-service.component.scss']
})
export class PartServiceComponent extends CommonService implements OnInit {
  data!: PartServiceList;
  page: number = 1;
  count: number = 0;
  limit: number = 0;
  showBoundaryLinks: boolean = true;
  showDirectionLinks: boolean = true;
  isCollapsed: boolean = true;

  constructor(public service: PartServService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initFormSearch();
    this.loadData();
    this.query();
  }

  loadData(): void {
    this.spinner.show();
    this.utils
      .get<PartServiceList>(`part-service?page=${this.page}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.data = res;
          this.limit = res.limit;
          this.count = res.count;
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.spinner.hide();
        }
      });
  }

  query(): void {
    this.service.formSearch
      .get('keyword')
      ?.valueChanges.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe({
        next: res => this.search(res),
        error: err => console.error(err)
      });
  }

  search(query: string): void {
    if (query.length > 0) {
      this.spinner.show();
      this.utils
        .get<any>(`part-service/search/${query}`)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.data = res;
            this.limit = res.limit;
            this.count = res.count;
            this.spinner.hide();
          },
          error: err => {
            console.error(err);
            this.spinner.hide();
          }
        });
    } else {
      this.loadData();
    }
  }

  pageCount(): number {
    return this.data ? this.data.count * this.data.limit : 0;
  }

  pageChanged(vl: any): void {
    this.page = vl.page;
    this.loadData();
  }

  delete(id: number): void {
    this.spinner.show();
    this.utils
      .delete(`part-service/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.spinner.hide();
          this.loadData();
          this.utils.toaster('Peça/Serviço removido com sucesso', 'success');
        },
        error: err => {
          this.spinner.hide();
          if (err.error) {
            this.utils.toaster(err.error, 'error');
          } else {
            this.utils.toaster('Ops!!! Algo saiu errado', 'error');
          }
        }
      });
  }

  messageEmptySearch(): boolean {
    if (this.data?.data) {
      return Object.keys(this.data?.data).length > 0 ? true : false;
    }
    return false;
  }
}
