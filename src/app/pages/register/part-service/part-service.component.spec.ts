import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartServiceComponent } from './part-service.component';

describe('PartServiceComponent', () => {
  let component: PartServiceComponent;
  let fixture: ComponentFixture<PartServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartServiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PartServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
