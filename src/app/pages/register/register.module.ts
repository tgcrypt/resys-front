import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/modules/shared.module';
import { EmployeeEditComponent } from './employee/employee-edit/employee-edit.component';
import { EmployeeComponent } from './employee/employee.component';
import { HomeComponent } from './home/home.component';
import { PartServiceEditComponent } from './part-service/part-service-edit/part-service-edit.component';
import { PartServiceComponent } from './part-service/part-service.component';
import { PartnerEditComponent } from './partner/partner-edit/partner-edit.component';
import { PartnerComponent } from './partner/partner.component';
import { ProviderEditComponent } from './provider/provider-edit/provider-edit.component';
import { ProviderComponent } from './provider/provider.component';
import { RegisterRoutingModule } from './register-routing.module';
import { RevisionEditComponent } from './revision/revision-edit/revision-edit.component';
import { RevisionComponent } from './revision/revision.component';
import { StockEditComponent } from './stock/stock-edit/stock-edit.component';
import { StockComponent } from './stock/stock.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { UserComponent } from './user/user.component';
import { VehicleEditComponent } from './vehicle/vehicle-edit/vehicle-edit.component';
import { VehicleComponent } from './vehicle/vehicle.component';

@NgModule({
  declarations: [
    HomeComponent,
    UserComponent,
    EmployeeComponent,
    VehicleComponent,
    PartnerComponent,
    ProviderComponent,
    PartServiceComponent,
    StockComponent,
    RevisionComponent,
    UserEditComponent,
    EmployeeEditComponent,
    PartServiceEditComponent,
    PartnerEditComponent,
    ProviderEditComponent,
    RevisionEditComponent,
    StockEditComponent,
    VehicleEditComponent
  ],
  imports: [CommonModule, RegisterRoutingModule, SharedModule]
})
export class RegisterModule {}
