import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardAdmin } from 'src/app/core/guards/admin.guard';
import { EmployeeEditComponent } from './employee/employee-edit/employee-edit.component';
import { EmployeeComponent } from './employee/employee.component';
import { HomeComponent } from './home/home.component';
import { PartServiceEditComponent } from './part-service/part-service-edit/part-service-edit.component';
import { PartServiceComponent } from './part-service/part-service.component';
import { PartnerEditComponent } from './partner/partner-edit/partner-edit.component';
import { PartnerComponent } from './partner/partner.component';
import { ProviderEditComponent } from './provider/provider-edit/provider-edit.component';
import { ProviderComponent } from './provider/provider.component';
import { RevisionEditComponent } from './revision/revision-edit/revision-edit.component';
import { RevisionComponent } from './revision/revision.component';
import { StockEditComponent } from './stock/stock-edit/stock-edit.component';
import { StockComponent } from './stock/stock.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { UserComponent } from './user/user.component';
import { VehicleEditComponent } from './vehicle/vehicle-edit/vehicle-edit.component';
import { VehicleComponent } from './vehicle/vehicle.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'user',
    component: UserComponent,
    canActivate: [AuthGuardAdmin]
  },
  {
    path: 'user/edit/:id',
    component: UserEditComponent,
    canActivate: [AuthGuardAdmin]
  },
  { path: 'employee', component: EmployeeComponent },
  { path: 'employee/edit/:id', component: EmployeeEditComponent },
  { path: 'part-service', component: PartServiceComponent },
  { path: 'part-service/edit/:id', component: PartServiceEditComponent },
  { path: 'partner', component: PartnerComponent },
  { path: 'partner/edit/:id', component: PartnerEditComponent },
  { path: 'provider', component: ProviderComponent },
  { path: 'provider/edit/:id', component: ProviderEditComponent },
  { path: 'revision', component: RevisionComponent },
  { path: 'revision/edit/:id', component: RevisionEditComponent },
  { path: 'stock', component: StockComponent },
  { path: 'stock/edit/:id', component: StockEditComponent },
  { path: 'vehicle', component: VehicleComponent },
  { path: 'vehicle/edit/:id', component: VehicleEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule {}
