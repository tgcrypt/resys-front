import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { take } from 'rxjs';
import { User } from 'src/app/shared/models/register/user.model';
import { UserService } from 'src/app/shared/services/register/user.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent extends CommonService implements OnInit {
  param!: string | null;
  title!: string;
  checkedAdmin: boolean = false;
  check: boolean = false;
  employeeList: { empId: number; empName: string }[] = [];

  constructor(public service: UserService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
    this.routeParam();
    this.loadSelectEmployee();
  }

  routeParam(): void {
    this.param = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.param && this.param != '0') {
      this.loadData(this.param);
    }
    this.setTitle(this.param);
  }

  setTitle(vl: string | null): void {
    if (vl != '0') {
      this.title = 'Editar Usuário';
    } else {
      this.title = 'Incluir Usuário';
    }
  }

  loadData(id: string): void {
    this.spinner.show();
    this.utils
      .get<User>(`user/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.service.setForm(res);
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          this.spinner.hide();
        }
      });
  }

  loadSelectEmployee(): void {
    this.spinner.show();
    this.utils
      .get<{ empId: number; empName: string }[]>('employee/select')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.employeeList = res;
          this.spinner.hide();
        },
        error: err => {
          this.spinner.hide();
          console.error(err);
          this.utils.toaster('Error ao carregar lista de colaborador', 'error');
        }
      });
  }

  submit(): void {
    this.spinner.show();
    const data: User = this.service.form.value;
    if (this.param != '0') {
      this.utils
        .put<User>(`user/${data.usrId}`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Usuário salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/user']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          }
        });
    } else {
      this.utils
        .post<User>(`user`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Usuário salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/user']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            if (err.error) {
              this.utils.toaster(err.error, 'error');
            } else {
              this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
            }
          }
        });
    }
  }

  isControlInvalid(controlName: string): boolean | undefined {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string): boolean {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }
}
