import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { take } from 'rxjs';
import { Stock } from 'src/app/shared/models/register/stock.model';
import { StockService } from 'src/app/shared/services/register/stock.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-stock-edit',
  templateUrl: './stock-edit.component.html',
  styleUrls: ['./stock-edit.component.scss']
})
export class StockEditComponent extends CommonService implements OnInit {
  param!: string | null;
  title!: string;
  checked: boolean = false;
  partServiceList: any;

  constructor(public service: StockService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
    this.routeParam();
    this.loadPartService();
  }

  routeParam(): void {
    this.param = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.param && this.param != '0') {
      this.loadData(this.param);
    }
    this.setTitle(this.param);
  }

  setTitle(vl: string | null): void {
    if (vl != '0') {
      this.title = 'Editar Peça / Revisão';
    } else {
      this.title = 'Incluir Peça / Revisão';
    }
  }

  loadData(id: string): void {
    this.spinner.show();
    this.utils
      .get<Stock>(`stock/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.service.setForm(res);
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          this.spinner.hide();
        }
      });
  }

  loadPartService(): void {
    this.utils
      .get<any>('part-service/select-part')
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.partServiceList = res;
        },
        error: err => {
          console.error(err);
        }
      });
  }

  loadDataPart(): void {
    const id = this.service.form.get('stkPartServiceId')?.value;
    if (!id) {
      const data = {
        ptsBrand: null,
        ptsCode: null,
        ptsName: null
      };
      this.service.setPartForm(data);
      return;
    }

    this.utils
      .get<any>(`part-service/part/data/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          console.log(res);
          this.service.setPartForm(res);
        }
      });
  }

  submit(): void {
    this.spinner.show();
    const data: Stock = this.service.form.value;
    if (this.param != '0') {
      this.utils
        .put<Stock>(`stock/${data.stkId}`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Stock salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/stock']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          }
        });
    } else {
      this.utils
        .post<Stock>(`stock`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Stock salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/stock']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            if (err.error) {
              this.utils.toaster(err.error, 'error');
            } else {
              this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
            }
          }
        });
    }
  }

  isControlInvalid(controlName: string): boolean | undefined {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string): boolean {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }
}
