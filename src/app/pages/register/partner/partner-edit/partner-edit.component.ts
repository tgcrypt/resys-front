import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { take } from 'rxjs';
import { Partner } from 'src/app/shared/models/register/partner.model';
import { PartnerService } from 'src/app/shared/services/register/partner.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-partner-edit',
  templateUrl: './partner-edit.component.html',
  styleUrls: ['./partner-edit.component.scss']
})
export class PartnerEditComponent extends CommonService implements OnInit {
  param!: string | null;
  title!: string;
  checked: boolean = false;
  types: { value: string; label: string }[] = [
    { value: 'Pessoal', label: 'Pessoal' },
    { value: 'Comercial', label: 'Comercial' }
  ];

  constructor(public service: PartnerService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
    this.routeParam();
  }

  routeParam(): void {
    this.param = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.param && this.param != '0') {
      this.loadData(this.param);
    }
    this.setTitle(this.param);
  }

  setTitle(vl: string | null): void {
    if (vl != '0') {
      this.title = 'Editar Parceiro';
    } else {
      this.title = 'Incluir Parceiro';
    }
  }

  loadData(id: string): void {
    this.spinner.show();
    this.utils
      .get<Partner>(`partner/${id}`)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.service.setForm(res);
          this.spinner.hide();
        },
        error: err => {
          console.error(err);
          this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          this.spinner.hide();
        }
      });
  }

  submit(): void {
    this.spinner.show();
    const data: Partner = this.service.form.value;
    if (this.param != '0') {
      this.utils
        .put<Partner>(`partner/${data.prtId}`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Parceiro salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/partner']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
          }
        });
    } else {
      this.utils
        .post<Partner>(`partner`, data)
        .pipe(take(1))
        .subscribe({
          next: res => {
            this.utils.toaster('Parceiro salvo com sucesso', 'success');
            this.spinner.hide();
            this.router.navigate(['/register/partner']);
          },
          error: err => {
            this.spinner.hide();
            console.error(err);
            if (err.error) {
              this.utils.toaster(err.error, 'error');
            } else {
              this.utils.toaster('Ops!!! Algo saiu errado.', 'error');
            }
          }
        });
    }
  }

  isControlInvalid(controlName: string): boolean | undefined {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string): boolean {
    const control: AbstractControl = this.service.form.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }
}
