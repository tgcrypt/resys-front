import { Component, Injector, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/admin/auth/auth.service';
import { CommonService } from 'src/app/shared/services/utils/common.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent extends CommonService implements OnInit {
  constructor(public service: AuthService, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.service.initForm();
  }
}
