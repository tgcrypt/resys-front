import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationConfig, PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsetConfig, TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxCurrencyModule } from 'ngx-currency';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PageTitleComponent } from 'src/app/core/components/template/page-title/page-title.component';
import { SearchMessageComponent } from '../components/search-message/search-message.component';
import { ToggleModule } from '../components/toggle-button/toggle-button.module';
import { BoolPipe } from '../pipes/bool.pipe';

const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    dropSpecialCharacters: false
  };
};

@NgModule({
  declarations: [PageTitleComponent, BoolPipe, SearchMessageComponent],
  imports: [
    CommonModule,
    NgxCurrencyModule,
    NgxMaskModule.forRoot(maskConfigFunction),
    NgxSpinnerModule,
    TooltipModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    SweetAlert2Module.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    NgSelectModule,
    TabsModule,
    ToggleModule.forRoot()
  ],
  exports: [
    NgxCurrencyModule,
    NgxMaskModule,
    NgxSpinnerModule,
    TooltipModule,
    PaginationModule,
    PageTitleComponent,
    CollapseModule,
    SweetAlert2Module,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    NgSelectModule,
    TabsModule,
    BoolPipe,
    SearchMessageComponent,
    ToggleModule
  ],
  providers: [PaginationConfig, TabsetConfig]
})
export class SharedModule {}
