export interface Auth {
  name: string;
  password: string;
}

export interface AuthResponse {
  id: string;
  name: string;
  user: string;
  email: string;
  admin: boolean;
  iat: number;
  exp: number;
  token: string;
}
