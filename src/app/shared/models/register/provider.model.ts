import { Common } from '../utils/common.model';
import { Email } from './email.model';
import { Phone } from './phone.model';

export interface Provider {
  prdId: number;
  prdName: string;
  prdRegister: string;
  prdDocument: string;
  prdAddress: string;
  prdNeighborhood: string;
  prdCity: string;
  prdState: string;
  prdPhoneId: number;
  prdEmailId: number;
  prdContact: string;
  prdStatus: boolean;
  prdEmail: Email;
  prdPhone: Phone;
}

export interface ProviderList extends Common {
  data: [
    {
      prdId: number;
      prdName: string;
      prdDocument: string;
      phnNumber: string;
      prdStatus: boolean;
      emlAddress: string;
    }
  ];
}
