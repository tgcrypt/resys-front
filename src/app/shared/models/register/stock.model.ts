import { Common } from '../utils/common.model';

export interface Stock {
  stkId: number;
  stkPartServiceId: number;
  stkQuantity: number;
  stkAmount: number;
  ptsBrand: string;
  ptsCode: string;
  ptsName: string;
}

export interface StockList extends Common {
  data: [
    {
      stkId: number;
      stkPartServiceId: number;
      stkQuantity: number;
      stkAmount: number;
      ptsBrand: string;
      ptsCode: string;
      ptsName: string;
    }
  ];
}
