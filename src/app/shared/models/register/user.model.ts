import { Common } from '../utils/common.model';

export interface User {
  usrId: number;
  usrUser: string;
  usrEmployeeId: number;
  usrStatus: boolean;
  usrAdministrator: boolean;
  usrPassword: string;
  usrConfirmPassword: string;
}

export interface UserList extends Common {
  data: [
    {
      usrId: number;
      usrUser: string;
      usrStatus: boolean;
      empName: string;
      empRegister: string;
      emlAddress: string;
    }
  ];
}
