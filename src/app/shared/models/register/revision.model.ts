import { Common } from '../utils/common.model';

export interface Revision {
  rvsId: number;
  rvsOrder: string;
  rvsVehicleId: number;
  rvsStartDate: Date;
  rvsFinalDate: Date;
  rvsKm: string;
  rvsEmployeeId: number;
  rvsObservation: string;
  rvsAmount: number;
  rvsTechnicalId: number;
  rvsPartServiceId: number[];
}

export interface RevisionList extends Common {
  data: [
    {
      rvsId: number;
      rvsOrder: string;
      rvsStartDate: Date;
      rvsFinalDate: Date;
      vhcLicensePlate: string;
      empName: string;
    }
  ];
}
