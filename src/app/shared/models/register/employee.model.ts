import { Common } from '../utils/common.model';
import { Email } from './email.model';
import { Phone } from './phone.model';

export interface Employee {
  empId: number;
  empName: string;
  empRegister: string;
  empDocument: string;
  empAddress: string;
  empNeighborhood: string;
  empCity: string;
  empState: string;
  empDepartment: string;
  empFunction: string;
  empStatus: boolean;
  empEmailId: number;
  empPhoneId: number;
  empEmail: Email;
  empPhone: Phone;
}

export interface EmployeeList extends Common {
  data: [
    {
      empId: number;
      empName: string;
      empFunction: string;
      empRegister: string;
      empStatus: boolean;
      emlAddress: string;
    }
  ];
}
