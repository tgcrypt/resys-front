import { Common } from '../utils/common.model';

export interface Vehicle {
  vhcId: number;
  vhcCode: string;
  vhcLicensePlate: string;
  vhcManufacture: string;
  vhcModel: string;
  vhcType: string;
  vhcLicenseState: string;
  vhcAcquisitionDate: Date;
  vhcDischargeDate: Date;
  vhcChassis: string;
  vhcRenavam: string;
  vhcIdDriver: number;
  vhcStatus: boolean;
}

export interface VehicleList extends Common {
  data: [
    {
      vhcId: number;
      vhcCode: string;
      vhcLicensePlate: string;
      vhcManufacture: string;
      vhcModel: string;
      vhcStatus: boolean;
    }
  ];
}
