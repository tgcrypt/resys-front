import { Common } from '../utils/common.model';
import { Email } from './email.model';
import { Phone } from './phone.model';

export interface Partner {
  prtId: number;
  prtName: string;
  prtRegister: string;
  prtDocument: string;
  prtAddress: string;
  prtNeighborhood: string;
  prtCity: string;
  prtState: string;
  prtContact: string;
  prtStatus: boolean;
  prtEmailId: number;
  prtPhoneId: number;
  prtEmail: Email;
  prtPhone: Phone;
}

export interface PartnerList extends Common {
  data: [
    {
      prtId: number;
      prtName: string;
      prtDocument: string;
      phnNumber: string;
      prtStatus: boolean;
      emlAddress: string;
    }
  ];
}
