import { Common } from '../utils/common.model';

export interface PartService {
  ptsId: number;
  ptsCode: string;
  ptsType: string;
  ptsName: string;
  ptsDescription: string;
  ptsBrand: string;
  ptsObservation: string;
  ptsUnity: string;
  ptsNew: boolean;
  ptsProviderId: string;
}

export interface PartServiceList extends Common {
  data: [
    {
      ptsId: number;
      ptsCode: string;
      ptsName: string;
      ptsBrand: string;
      ptsProviderId: number;
      ptsType: boolean;
      prdName: string;
    }
  ];
}
