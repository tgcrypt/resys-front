export interface Phone {
  phnType: string;
  phnNumber: string;
}
