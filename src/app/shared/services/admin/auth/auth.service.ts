import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { take } from 'rxjs/operators';
import { AuthState } from 'src/app/core/store/auth/auth.reduce';
import * as AuthActions from '../../../../core/store/auth/auth.actions';
import { Auth, AuthResponse } from '../../../models/admin/auth/auth.model';
import { UtilsService } from '../../utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  form!: FormGroup;

  constructor(
    private store: Store<{ auth: AuthState }>,
    public fb: FormBuilder,
    private utils: UtilsService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  initForm(): void {
    this.form = this.fb.group({
      name: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  setForm(data: Auth): void {
    this.form.patchValue({
      name: data.name,
      password: data.password
    });
  }

  login(): void {
    this.spinner.show();
    this.utils
      .post<AuthResponse>('sign-in', this.form.value)
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.store.dispatch(
            AuthActions.setToken({ token: res.token, user: res.name })
          );
          this.utils.setLocalStorage(res);
          this.spinner.hide();
          this.router.navigate(['/']);
        },
        error: err => {
          this.spinner.hide();
          console.error(err);
          if (typeof err.error != 'string') {
            this.utils.toaster('Ops!!! Algo deu errado.', 'error');
          } else this.utils.toaster(err.error, 'error');
        }
      });
  }

  logout(): void {
    this.store.dispatch(AuthActions.cleanToken());
    this.utils.removeLocalStorage();
    this.router.navigate(['sign-in']);
  }

  validateLogin() {
    const data = this.utils.getLocalStorage();
    if (!data || !data.token) {
      return false;
    } else {
      this.store.dispatch(
        AuthActions.setToken({ token: data.token, user: data.user })
      );
      return true;
    }
  }

  validateAdmin() {
    const data = this.utils.getLocalStorage();
    if (!data || !data.admin) {
      return false;
    } else {
      return true;
    }
  }
}
