import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PartService } from '../../models/register/part-service.model';
import { Stock } from '../../models/register/stock.model';

@Injectable({
  providedIn: 'root'
})
export class StockService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      stkId: [null],
      stkPartServiceId: [null, Validators.required],
      stkQuantity: [null, Validators.required],
      stkAmount: [null, Validators.required],
      ptsBrand: [null],
      ptsCode: [null],
      ptsName: [null]
    });
  }

  setForm(data: Stock): void {
    this.form.patchValue({
      stkId: data.stkId,
      stkPartServiceId: data.stkPartServiceId,
      stkQuantity: data.stkQuantity,
      stkAmount: data.stkAmount,
      ptsBrand: data.ptsBrand,
      ptsCode: data.ptsCode,
      ptsName: data.ptsName
    });
  }

  setPartForm(data: any): void {
    this.form.patchValue({
      ptsBrand: data.ptsBrand,
      ptsCode: data.ptsCode,
      ptsName: data.ptsName
    });
  }
}
