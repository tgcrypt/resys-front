import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from '../../models/register/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      empId: [null],
      empName: [null, Validators.required],
      empRegister: [null, Validators.required],
      empDocument: [null, Validators.required],
      empAddress: [null, Validators.required],
      empNeighborhood: [null, Validators.required],
      empCity: [null, Validators.required],
      empState: [null, Validators.required],
      empDepartment: [null, Validators.required],
      empFunction: [null, Validators.required],
      empStatus: [null, Validators.required],
      empEmailId: [null],
      empPhoneId: [null],
      empEmail: this.fb.group({
        emlType: [null],
        emlAddress: [null]
      }),
      empPhone: this.fb.group({
        phnType: [null],
        phnNumber: [null]
      })
    });
  }

  setForm(data: Employee): void {
    this.form.patchValue({
      empId: data.empId,
      empName: data.empName,
      empRegister: data.empRegister,
      empDocument: data.empDocument,
      empAddress: data.empAddress,
      empNeighborhood: data.empNeighborhood,
      empCity: data.empCity,
      empState: data.empState,
      empDepartment: data.empDepartment,
      empFunction: data.empFunction,
      empStatus: data.empStatus,
      empEmailId: data.empEmailId,
      empPhoneId: data.empPhoneId,
      empEmail: {
        emlType: data.empEmail.emlType,
        emlAddress: data.empEmail.emlAddress
      },
      empPhone: {
        phnType: data.empPhone.phnType,
        phnNumber: data.empPhone.phnNumber
      }
    });
  }
}
