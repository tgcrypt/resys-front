import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Revision } from '../../models/register/revision.model';
import { Vehicle } from '../../models/register/vehicle.model';
import { UtilsService } from '../utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class RevisionService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder, public utils: UtilsService) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      rvsId: [null],
      rvsOrder: [null, Validators.required],
      rvsKm: [null, Validators.required],
      rvsVehicleId: [null, Validators.required],
      rvsEmployeeId: [null, Validators.required],
      rvsStartDate: [null, Validators.required],
      rvsFinalDate: [null],
      rvsObservation: [null],
      rvsTechnicalId: [null, Validators.required],
      rvsAmount: [null, Validators.required],
      rvsPartServiceId: this.fb.array([
        this.fb.control(null, Validators.required)
      ])
    });
  }

  setForm(data: Revision): void {
    if (data.rvsPartServiceId?.length > 0) {
      this.deleteRvsPartServiceId(0);
      data.rvsPartServiceId.forEach(vl =>
        this.rvsPartServiceId.push(this.fb.control(vl))
      );
    }
    this.form.patchValue({
      rvsId: data.rvsId,
      rvsOrder: data.rvsOrder,
      rvsKm: data.rvsKm,
      rvsVehicleId: data.rvsVehicleId,
      rvsEmployeeId: data.rvsEmployeeId,
      rvsStartDate: this.utils.formatDate(data.rvsStartDate),
      rvsFinalDate: this.utils.formatDate(data.rvsFinalDate),
      rvsObservation: data.rvsObservation,
      rvsAmount: data.rvsAmount,
      rvsTechnicalId: data.rvsTechnicalId
    });
  }

  get rvsPartServiceId(): FormArray {
    return this.form.get('rvsPartServiceId') as FormArray;
  }

  addRvsPartServiceId(): void {
    this.rvsPartServiceId.push(this.fb.control(null, Validators.required));
  }

  deleteRvsPartServiceId(idx: number) {
    this.rvsPartServiceId.removeAt(idx);
  }
}
