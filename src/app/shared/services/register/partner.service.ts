import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from '../../models/register/employee.model';
import { Partner } from '../../models/register/partner.model';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      prtId: [null],
      prtName: [null, Validators.required],
      prtRegister: [null, Validators.required],
      prtDocument: [null, Validators.required],
      prtAddress: [null, Validators.required],
      prtNeighborhood: [null, Validators.required],
      prtCity: [null, Validators.required],
      prtState: [null, Validators.required],
      prtContact: [null, Validators.required],
      prtStatus: [false, Validators.required],
      prtEmailId: [null],
      prtPhoneId: [null],
      prtEmail: this.fb.group({
        emlType: [null],
        emlAddress: [null]
      }),
      prtPhone: this.fb.group({
        phnType: [null],
        phnNumber: [null]
      })
    });
  }

  setForm(data: Partner): void {
    this.form.patchValue({
      prtId: data.prtId,
      prtName: data.prtName,
      prtRegister: data.prtRegister,
      prtDocument: data.prtDocument,
      prtAddress: data.prtAddress,
      prtNeighborhood: data.prtNeighborhood,
      prtCity: data.prtCity,
      prtState: data.prtState,
      prtContact: data.prtContact,
      prtStatus: data.prtStatus,
      prtEmailId: data.prtEmailId,
      prtPhoneId: data.prtPhoneId,
      prtEmail: {
        emlType: data.prtEmail.emlType,
        emlAddress: data.prtEmail.emlAddress
      },
      prtPhone: {
        phnType: data.prtPhone.phnType,
        phnNumber: data.prtPhone.phnNumber
      }
    });
  }
}
