import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PartService } from '../../models/register/part-service.model';
import { Vehicle } from '../../models/register/vehicle.model';

@Injectable({
  providedIn: 'root'
})
export class PartServService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      ptsId: [null],
      ptsCode: [null, Validators.required],
      ptsType: [null, Validators.required],
      ptsName: [null, Validators.required],
      ptsDescription: [null, Validators.required],
      ptsBrand: [null, Validators.required],
      ptsObservation: [null, Validators.required],
      ptsUnity: [null, Validators.required, Validators.maxLength(2)],
      ptsNew: [null, Validators.required],
      ptsProviderId: [null, Validators.required]
    });
  }

  setForm(data: PartService): void {
    this.form.patchValue({
      ptsId: data.ptsId,
      ptsCode: data.ptsCode,
      ptsType: data.ptsType,
      ptsName: data.ptsName,
      ptsDescription: data.ptsDescription,
      ptsBrand: data.ptsBrand,
      ptsObservation: data.ptsObservation,
      ptsUnity: data.ptsUnity,
      ptsNew: data.ptsNew,
      ptsProviderId: data.ptsProviderId
    });
  }
}
