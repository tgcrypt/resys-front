import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Vehicle } from '../../models/register/vehicle.model';
import { UtilsService } from '../utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder, public utils: UtilsService) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      vhcId: [null],
      vhcCode: [null, Validators.required],
      vhcLicensePlate: [null, Validators.required],
      vhcManufacture: [null, Validators.required],
      vhcModel: [null, Validators.required],
      vhcType: [null, Validators.required],
      vhcLicenseState: [null, Validators.required],
      vhcAcquisitionDate: [null, Validators.required],
      vhcDischargeDate: [null],
      vhcChassis: [null, Validators.required],
      vhcRenavam: [null, Validators.required],
      vhcIdDriver: [null, Validators.required],
      vhcStatus: [null, Validators.required]
    });
  }

  setForm(data: Vehicle): void {
    this.form.patchValue({
      vhcId: data.vhcId,
      vhcCode: data.vhcCode,
      vhcLicensePlate: data.vhcLicensePlate,
      vhcManufacture: data.vhcManufacture,
      vhcModel: data.vhcModel,
      vhcType: data.vhcType,
      vhcLicenseState: data.vhcLicenseState,
      vhcAcquisitionDate: this.utils.formatDate(data.vhcAcquisitionDate),
      vhcDischargeDate: this.utils.formatDate(data.vhcDischargeDate),
      vhcChassis: data.vhcChassis,
      vhcRenavam: data.vhcRenavam,
      vhcIdDriver: data.vhcIdDriver,
      vhcStatus: data.vhcStatus
    });
  }
}
