import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Provider } from '../../models/register/provider.model';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      prdId: [null],
      prdName: [null, Validators.required],
      prdRegister: [null, Validators.required],
      prdDocument: [null, Validators.required],
      prdAddress: [null, Validators.required],
      prdNeighborhood: [null, Validators.required],
      prdCity: [null, Validators.required],
      prdState: [null, Validators.required],
      prdPhoneId: [null],
      prdEmailId: [null],
      prdContact: [null, Validators.required],
      prdStatus: [null, Validators.required],
      prdEmail: this.fb.group({
        emlType: [null],
        emlAddress: [null]
      }),
      prdPhone: this.fb.group({
        phnType: [null],
        phnNumber: [null]
      })
    });
  }

  setForm(data: Provider): void {
    this.form.patchValue({
      prdId: data.prdId,
      prdName: data.prdName,
      prdRegister: data.prdRegister,
      prdDocument: data.prdDocument,
      prdAddress: data.prdAddress,
      prdNeighborhood: data.prdNeighborhood,
      prdCity: data.prdCity,
      prdState: data.prdState,
      prdPhoneId: data.prdPhoneId,
      prdEmailId: data.prdEmailId,
      prdContact: data.prdContact,
      prdStatus: data.prdStatus,
      prdEmail: {
        emlType: data.prdEmail.emlType,
        emlAddress: data.prdEmail.emlAddress
      },
      prdPhone: {
        phnType: data.prdPhone.phnType,
        phnNumber: data.prdPhone.phnNumber
      }
    });
  }
}
