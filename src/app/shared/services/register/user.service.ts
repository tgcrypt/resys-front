import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/register/user.model';
import { ConfirmPasswordValidatorUser } from '../utils/confirm-password.validator';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  form!: FormGroup;
  formSearch!: FormGroup;

  constructor(public fb: FormBuilder) {}

  initFormSearch(): void {
    this.formSearch = this.fb.group({
      keyword: [null]
    });
  }

  initForm(): void {
    this.form = this.fb.group(
      {
        usrId: [null],
        usrUser: [null, Validators.required],
        usrEmployeeId: [null, Validators.required],
        usrStatus: [false, Validators.required],
        usrAdministrator: [false, Validators.required],
        usrPassword: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(16)
          ])
        ],
        usrConfirmPassword: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(16)
          ])
        ]
      },
      {
        validator: ConfirmPasswordValidatorUser.MatchPassword
      }
    );
  }

  setForm(data: User): void {
    this.form.patchValue({
      usrId: data.usrId,
      usrUser: data.usrUser,
      usrEmployeeId: data.usrEmployeeId,
      usrStatus: data.usrStatus,
      usrAdministrator: data.usrAdministrator
    });
  }
}
