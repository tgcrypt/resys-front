import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor(protected http: HttpClient) {}

  get<T>(args: string): Observable<T> {
    return this.http.get<T>(`${environment.api}${args}`);
  }

  post<T>(arg: string, data: any): Observable<T> {
    return this.http.post<T>(`${environment.api}${arg}`, data);
  }

  put<T>(arg: string, data: any): Observable<T> {
    return this.http.put<T>(`${environment.api}${arg}`, data);
  }

  delete<T>(arg: string): Observable<T> {
    return this.http.delete<T>(`${environment.api}${arg}`);
  }

  formatDate(value: any) {
    if (value) {
      let newValue = value.split('T')[0].split('-');
      value = `${newValue[0]}-${newValue[1]}-${newValue[2]}`;
      return value;
    } else {
      return;
    }
  }

  getLocalStorage() {
    return JSON.parse(localStorage.getItem('__system_user') as string);
  }

  removeLocalStorage() {
    localStorage.removeItem('__system_user');
  }

  setLocalStorage(data: any) {
    localStorage.setItem('__system_user', JSON.stringify(data));
  }

  toaster(title: string, icon: any): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: toast => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      }
    });

    Toast.fire({
      icon: icon,
      title: title
    });
  }
}
