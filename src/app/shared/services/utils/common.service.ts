import { ChangeDetectorRef, Injectable, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export abstract class CommonService {
  protected utils: UtilsService;
  protected spinner: NgxSpinnerService;
  protected changerDetector: ChangeDetectorRef;
  protected activatedRoute: ActivatedRoute;
  protected router: Router;

  constructor(protected injector: Injector) {
    this.utils = this.injector.get(UtilsService);
    this.spinner = this.injector.get(NgxSpinnerService);
    this.changerDetector = this.injector.get(ChangeDetectorRef);
    this.activatedRoute = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
  }
}
