import { AbstractControl } from '@angular/forms';

export class ValidatorCpfCnpj {
  constructor() {}

  static cpfCnpj(control: AbstractControl) {
    const documentType: string = control.get('sprType')?.value;
    let cpfCnpj = control.get('sprCnpjCpf')?.value;
    if (cpfCnpj && documentType == 'F' && Object.values(cpfCnpj).length == 14) {
      const cpf = cleaner(cpfCnpj);
      const firstNineDigits = cpf.substring(0, 9);
      const checker = cpf.substring(9, 11);
      for (let i: any = 0; i < 10; i++) {
        if (firstNineDigits + checker === Array(12).join(i)) {
          return control.get('sprCnpjCpf')?.setErrors({ CnpjCpfError: true });
        }
      }

      const checker1 = cpfCalcChecker1(firstNineDigits);
      const checker2 = cpfCalcChecker2(`${firstNineDigits}${checker1}`);
      if (checker.toString() != checker1.toString() + checker2.toString()) {
        return control.get('sprCnpjCpf')?.setErrors({ CnpjCpfError: true });
      }
    }

    if (cpfCnpj && documentType == 'J' && Object.values(cpfCnpj).length == 18) {
      const cnpj = cleaner(cpfCnpj);
      const pesosDigito1 = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
      const pesosDigito2 = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
      for (let i: any = 0; i < 10; i++) {
        if (cnpj === Array(15).join(i)) {
          return control.get('sprCnpjCpf')?.setErrors({ CnpjCpfError: true });
        }
      }
      if (
        !checkDigitCnpj(cnpj, pesosDigito1) ||
        !checkDigitCnpj(cnpj, pesosDigito2)
      ) {
        return control.get('sprCnpjCpf')?.setErrors({ CnpjCpfError: true });
      }
    }
  }
}

// funções de validação de CPF
function cpfCalcChecker1(firstNineDigits: any): any {
  let sum: any = 0;

  for (let j = 0; j < 9; ++j) {
    sum += firstNineDigits.toString().charAt(j) * (10 - j);
  }

  const lastSumChecker1 = sum % 11;
  const checker1 = lastSumChecker1 < 2 ? 0 : 11 - lastSumChecker1;
  return checker1;
}

function cpfCalcChecker2(cpfWithChecker1: any): any {
  let sum: any = 0;

  for (let k = 0; k < 10; ++k) {
    sum += cpfWithChecker1.toString().charAt(k) * (11 - k);
  }
  const lastSumChecker2 = sum % 11;
  const checker2 = lastSumChecker2 < 2 ? 0 : 11 - lastSumChecker2;

  return checker2;
}

// funções de validação de CNPJ

function checkDigitCnpj(cnpj: any, pesos: any) {
  let numbers = cnpj.split('').slice(0, pesos.length);
  // Soma numeros do CNPJ baseado nos pesos
  let acumuladora = numbers.reduce((anterior: any, atual: any, index: any) => {
    return anterior + atual * pesos[index];
  }, 0);
  let resto = acumuladora % 11;
  let digito = resto < 2 ? 0 : 11 - resto;
  return parseInt(cnpj[pesos.length]) === digito;
}

function cleaner(value: any): any {
  return value.replace(/[^\d]/g, '');
}
