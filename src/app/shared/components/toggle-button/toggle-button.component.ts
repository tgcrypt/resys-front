import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-toggle-button',
  templateUrl: './toggle-button.component.html',
  styleUrls: ['./toggle-button.component.scss']
})
export class ToggleButtonComponent {
  @Input() check: boolean = false;
  @Output() changed = new EventEmitter();
  constructor() {}

  onChange(event: any) {
    this.changed.emit(event.target.checked);
  }
}
