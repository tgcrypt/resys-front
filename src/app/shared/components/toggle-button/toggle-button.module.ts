import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ToggleButtonComponent } from './toggle-button.component';

@NgModule({
  declarations: [ToggleButtonComponent],
  imports: [CommonModule],
  exports: [ToggleButtonComponent]
})
export class ToggleModule {
  static forRoot(): ModuleWithProviders<ToggleModule> {
    return {
      ngModule: ToggleModule,
      providers: []
    };
  }
}
